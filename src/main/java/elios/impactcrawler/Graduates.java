package elios.impactcrawler;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.logging.Level;

public class Graduates implements Serializable {
    private final Integer year;
    private final Integer number;

    public Graduates(Integer year, Integer number) {
        this.year = year;
        this.number = number;
    }

    public Integer getYear() { return year; }
    public Integer getNumber() { return number; }

    
    @Override
    public String toString()  {
        String description = "Graduates{";
        Field[] fields = Graduates.class.getDeclaredFields();
        for (Field field : fields) {
            try {
                if(field.get(this) != null) description += " " + field.getName() + ":" + field.get(this) + ";";
            } 
            catch (IllegalArgumentException ex) { java.util.logging.Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex); } 
            catch (IllegalAccessException ex) { java.util.logging.Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex); }
        }
        description += "}";
        return description;
    }
}
