package elios.impactcrawler;

public class Request {
    private final String ssd_name;
    private final String ssd_id;
    private final String degree_id;

    public Request(String ssd_name, String ssd_id, String degree_id) {
        this.ssd_name = ssd_name;
        this.ssd_id = ssd_id;
        this.degree_id = degree_id;
    }

    public String getSSD_name() { return ssd_name; }
    public String getSSD_id() { return ssd_id; }
    public String getDegree_id() { return degree_id; }
}
