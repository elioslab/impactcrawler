package elios.impactcrawler.diten;

import elios.impactcrawler.Unit;
import elios.impactcrawler.Evaluator;
import elios.impactcrawler.Logger;
import elios.impactcrawler.Member;
import elios.impactcrawler.Metric;
import elios.impactcrawler.Paper;
import elios.impactcrawler.Scopus;
import elios.impactcrawler.Status;
import elios.impactcrawler.Utils;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class diten {

    public static void main(String[] args) throws Exception {

        Logger.setVerbose(true);

        File output_dir = new File("output"); output_dir.mkdir();
        File logs_dir = new File("output/logs"); logs_dir.mkdir();

        // Units
        List<Unit> units = new ArrayList<>();
        //units.add(new Unit("TEST"));
        units.add(new Unit("ICT"));
        units.add(new Unit("ELE"));
        units.add(new Unit("NAV"));
        //units.add(new Unit("OTHERS"));
        
        //Metrics
        List<String> metric_names = new ArrayList<>();
        List<Integer> start_dates = new ArrayList<>();
        List<Integer> end_dates = new ArrayList<>();
        metric_names.add("absolute");
        start_dates.add(1900);
        end_dates.add(2019);
        metric_names.add("05 years");
        start_dates.add(2014);
        end_dates.add(2018);
        metric_names.add("10 years");
        start_dates.add(2009);
        end_dates.add(2018);
        metric_names.add("15 years");
        start_dates.add(2004);
        end_dates.add(2018);
        
        //Trends
        
        //Execute evaluation
        for(int i=0; i<units.size(); i++) {
            String unit_name = units.get(i).getName();
            setUnit(unit_name);
            getPapers(unit_name);
            evaluateMetrics(unit_name, metric_names, start_dates, end_dates);
            exportJSON_CSV_Charts(unit_name);
        }
    }
    
    public static void setUnit(String unit_name) throws Exception {
        Logger.setFileName(null);
        Logger.write("Setting unit " + unit_name);
 
        Unit unit = new Unit(unit_name);
        if (!unit.getStatus().equals(Status.empty)) { Logger.write("Already available"); return; }
        Logger.write("Setting information...");
 
        List<List<String>> records = new ArrayList<>();
        try {
            InputStream inputStream = diten.class.getClassLoader().getResourceAsStream(unit_name + ".csv");
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            String line;
            while ((line = br.readLine()) != null) {
                String[] values = line.split(";");
                records.add(Arrays.asList(values));
            }
        }
        catch(IOException ex) { Logger.write("Error: " + ex.getMessage()); }
        
        List<Member> members = new ArrayList<>();
        for(int i=0; i<records.size(); i++) {
            List<String> element = records.get(i);
            String surname = element.get(0);
            String name = element.get(1);
            String role = element.get(2);
            String ssd = element.get(3);
            String scopus = element.get(4);
            members.add(new Member(surname, name, role, ssd, scopus));
        }
        unit.setMembers(members);
        unit.setStatus(Status.scopusid);
        unit.save();
    }
    
    public static void getPapers(String unit_name) throws Exception {
        Logger.setFileName(null);
        Logger.write("Searching for papers from Scopus");
        Scopus scopus = new Scopus();
        Unit unit = new Unit(unit_name);
        if (!unit.getStatus().equals(Status.scopusid)) { Logger.write("Already available"); return; }
        Logger.write("Crawling information...");        
        Logger.setFileName("output/logs/log_papers_scopus_" + unit.getName() + ".txt");
        List<Member> list = unit.getMembers();
        for (int i = 0; i < list.size(); i++) {
            Member member = list.get(i);
            Logger.write("[" + i + "] " + member.toString());
            if (member.getScopusid() != null) {
                List<Paper> papers = scopus.getPapers(member.getScopusid());
                member.setPapers(papers);
            }
        }
        unit.setStatus(Status.scopuspaper);
        unit.save();
    }

    public static void evaluateMetrics(String unit_name, List<String> metric_names, List<Integer> start_dates, List<Integer> end_dates) throws Exception {
        Logger.setFileName(null);
        Logger.write("Evaluating  metric");
        Unit unit = new Unit(unit_name);
        Logger.write("Calculating information...");
        Logger.setFileName("output/logs/log_metrics_evaluation_" + unit.getName() + ".txt");
        List<Member> list = unit.getMembers();
        for (int i = 0; i < list.size(); i++) {
            List<Metric> metrics = new ArrayList<>();
            Member member = list.get(i);
            Evaluator evaluator = new Evaluator(unit);
            
            Logger.write("[" + i + "] " + member.toString());
            for(int j=0; j<metric_names.size(); j++) {
                Metric evaluated = evaluator.metric(member, metric_names.get(j), start_dates.get(j), end_dates.get(j));
                metrics.add(evaluated);
                Logger.write(" - " + metric_names.get(j) + " " + evaluated.toString());
            }
            Logger.write(" ");
            member.setMetrics(metrics);
        }
        unit.setStatus(Status.metrics);
        unit.save();
    }

    public static void exportJSON_CSV_Charts(String unit_name) throws IOException, Exception {
        Logger.setFileName(null);
        Logger.write("Exporting to JSON, CSV and Charts");
        Unit unit = new Unit(unit_name);
        Logger.write(" - to JSON...");
        Utils.toJSONFile(unit);
        Logger.write(" - to CSV...");
        Utils.toCSVFileMembers(unit);
        unit.setStatus(Status.complete);
        unit.save();
        Logger.write("Complete");
    }
}
