package elios.impactcrawler;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.logging.Level;

public class Metric implements Serializable
{
    private final String name;
    private final Integer hindex;
    private final Integer documentCount;
    private final Integer journalDocumentCount;
    private final Integer conferenceDocumentCount;
    private final Integer citationCount;
    private final Integer citationFromApplepies;
    private final Integer startYear;
    private final Integer stopYear;

    public Metric(String name, Integer hindex, Integer documentCount, Integer journalDocumentCount, Integer conferenceDocumentCount, Integer citationCount, Integer citationFromApplepies, Integer startYear, Integer stopYear) {
        this.name = name;
        this.hindex = hindex;
        this.documentCount = documentCount;
        this.journalDocumentCount = journalDocumentCount;
        this.conferenceDocumentCount = conferenceDocumentCount;
        this.citationCount = citationCount;
        this.citationFromApplepies = citationFromApplepies;
        this.startYear = startYear;
        this.stopYear = stopYear;
    }

    public String getName() { return name; }
    public Integer getHindex() { return hindex; }
    public Integer getDocumentCount() { return documentCount; }
    public Integer getCitationCount() { return citationCount; }
    public Integer getCitationFromApplepies()  { return citationFromApplepies; }
    public Integer getStartYear() { return startYear; }
    public Integer getStopYear() { return stopYear; }
    public Integer getJournalDocumentCount() { return journalDocumentCount; }
    public Integer getConferenceDocumentCount() { return conferenceDocumentCount; }
    
    @Override
    public String toString()  {
        String description = "Metric{";
        Field[] fields = Metric.class.getDeclaredFields();
        for (Field field : fields) {
            try {
                if(field.get(this) != null) {
                    if(field.getGenericType() == String.class) description += " " + field.getName() + ":" + (String)field.get(this) + ";";
                    else description += " " + field.getName() + ":" + (Integer)field.get(this) + ";";
                }
            } 
            catch (IllegalArgumentException ex) { java.util.logging.Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex); } 
            catch (IllegalAccessException ex) { java.util.logging.Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex); }
        }
        description += "}";
        return description;
    }
}
