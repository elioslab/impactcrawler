package elios.impactcrawler;

import java.util.List;

public interface Aggregate {
    public String getName();
    public void setMembers(List<Member> members);
    public List<Member> getMembers();
    public List<Metric> getMedians();
    public void setMedians(List<Metric> medians);
    public List<Distribution> getFrequencies();
    public void setFrequencies(List<Distribution> frequencies);
    public List<Distribution> getPercentiles();
    public void setPercentiles(List<Distribution> percentiles);
    public void setStatus(Status status);
    public Status getStatus();
    public void load();
    public void save(); 
}
