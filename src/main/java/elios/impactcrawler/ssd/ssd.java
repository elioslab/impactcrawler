package elios.impactcrawler.ssd;

import elios.impactcrawler.Cineca;
import elios.impactcrawler.Distribution;
import elios.impactcrawler.Evaluator;
import elios.impactcrawler.Logger;
import elios.impactcrawler.Member;
import elios.impactcrawler.Metric;
import elios.impactcrawler.Miur;
import elios.impactcrawler.Paper;
import elios.impactcrawler.Request;
import elios.impactcrawler.SSD;
import elios.impactcrawler.Scopus;
import elios.impactcrawler.Status;
import elios.impactcrawler.Utils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ssd {
    
    public static void main(String[] args) throws Exception {
        
        List<Request> requests = new ArrayList<>();
        requests.add(new Request("Elettronica", "ING-INF/01", "LM-29"));
        //requests.add(new Request("Campi", "ING-INF/02", null));
        //requests.add(new Request("Telecomunicazioni", "ING-INF/03", "LM-27"));
        //requests.add(new Request("Automatica", "ING-INF/04", "LM-25"));
        //requests.add(new Request("IngegneriaInformatica", "ING-INF/05", "LM-32"));
        //requests.add(new Request("Bioingegneria", "ING-INF/06", "LM-21"));
        //requests.add(new Request("Misure", "ING-INF/07", null));
        //requests.add(new Request("InformaticaScienze", "INF/01", "LM-18"));
        
        Logger.setVerbose(true);

        File dir = new File("output"); dir.mkdir();
        dir = new File("output/images"); dir.mkdir();
        dir = new File("output/frequencies"); dir.mkdir();
        dir = new File("output/percentiles"); dir.mkdir();
        dir = new File("output/logs"); dir.mkdir();
        dir = new File("output/graduates"); dir.mkdir();

        for(int i=0; i<requests.size(); i++) {
            Logger.write(" ");
            Logger.write(requests.get(i).getSSD_name() + " (" + requests.get(i).getSSD_id() + ")");
            getGraduates(requests.get(i));
            getUsers(requests.get(i));
            getUserIDs(requests.get(i));
            getPapers(requests.get(i));
            evaluateMetrics(requests.get(i));
            evaluateMedians(requests.get(i));
            evaluateFrequencies(requests.get(i));
            evaluatePercentile(requests.get(i));
            exportJSON_CSV_Charts(requests.get(i));
        }
    }
    
    public static void getGraduates(Request request) throws Exception {
        Logger.setFileName(null);
        Logger.write("Extracting graduates from Miur");
        Miur miur = new Miur();
        SSD ssd = new SSD(request.getSSD_name(), request.getSSD_id(), request.getDegree_id());
        if (!ssd.getStatus().equals(Status.empty)) { Logger.write("Already available"); return; }
        Logger.write("Crawling information...");
        Logger.setFileName("output/logs/log_graduates_miur_" + request.getSSD_name() + ".txt");
        ssd.setGraduates(miur.getGraduatesNumber(request.getDegree_id()));
        ssd.setStatus(Status.miur);
        Logger.write("Graduates: " + ssd.getGraduates().toString());
        ssd.save();
    } 
    
    public static void getUsers(Request request) throws Exception {
        Logger.setFileName(null);
        Logger.write("Extracting users from Cineca");
        Cineca cineca = new Cineca();
        SSD ssd = new SSD(request.getSSD_name(), request.getSSD_id(), request.getDegree_id());
        if (!ssd.getStatus().equals(Status.miur)) { Logger.write("Already available"); return; }
        Logger.write("Crawling information...");
        Logger.setFileName("output/logs/log_members_cineca_" + request.getSSD_name() + ".txt");
        ssd.setMembers(cineca.getMembers(request.getSSD_id()));
        ssd.setStatus(Status.cineca);
        ssd.save();
    } 
    
    public static void getUserIDs(Request request) throws Exception {
        Logger.setFileName(null);
        Logger.write("Searching for authorIDs from Scopus");
        Scopus scopus = new Scopus();
        SSD ssd = new SSD(request.getSSD_name(), request.getSSD_id(), request.getDegree_id());
        if (!ssd.getStatus().equals(Status.cineca)) { Logger.write("Already available"); return; }
        Logger.write("Crawling information...");
        Logger.setFileName("output/logs/log_ids_scopus_" + request.getSSD_name() + ".txt");
        List<Member> list = ssd.getMembers();
        for (int i = 0; i < list.size(); i++) {
            Member member = list.get(i);
            Logger.write("[" + i + "] " + member.toString());
            String scopus_id_from_scopus = scopus.getScopusID(member.getName(), member.getSurname(), member.getUniversity());
            if (scopus_id_from_scopus == null) Logger.warning("Not found on Scopus: " + member.getName() + " " + member.getSurname());
            member.setScopusid(scopus_id_from_scopus);
        }
        ssd.setStatus(Status.scopusid);
        ssd.save();
    }
    
    public static void getPapers(Request request) throws Exception {
        Logger.setFileName(null);
        Logger.write("Searching for papers from Scopus");
        Scopus scopus = new Scopus();
        SSD ssd = new SSD(request.getSSD_name(), request.getSSD_id(), request.getDegree_id());
        if (!ssd.getStatus().equals(Status.scopusid)) { Logger.write("Already available"); return; }
        Logger.write("Crawling information...");
        Logger.setFileName("output/logs/log_papers_scopus_"  + request.getSSD_name() + ".txt");
        List<Member> list = ssd.getMembers();
        for (int i = 0; i < list.size(); i++) {
            Member member = list.get(i);
            Logger.write("[" + i + "] " + member.toString());
            if (member.getScopusid() != null) {
                List<Paper> papers = scopus.getPapers(member.getScopusid());
                member.setPapers(papers);
            }
        }
        ssd.setStatus(Status.scopuspaper);
        ssd.save();
    }

    public static void evaluateMetrics(Request request) throws Exception {
        Logger.setFileName(null);
        Logger.write("Evaluating  metric");
        SSD ssd = new SSD(request.getSSD_name(), request.getSSD_id(), request.getDegree_id());
        if (!ssd.getStatus().equals(Status.scopuspaper)) { Logger.write("Already available"); return; }
        Logger.write("Calculating information...");
        Logger.setFileName("output/logs/log_metrics_evaluation_"  + request.getSSD_name() + ".txt");
        List<Member> list = ssd.getMembers();
        for (int i = 0; i < list.size(); i++) {
            List<Metric> metrics = new ArrayList<>();
            Member member = list.get(i);
            Evaluator evaluator = new Evaluator(ssd);
            Metric evaluated_absolute = evaluator.metric(member, "absolute", 1900, 2019);
            Metric evaluated_05 = evaluator.metric(member, "05 years", 2015, 2019);
            Metric evaluated_10 = evaluator.metric(member, "10 years", 2008, 2019);
            Metric evaluated_15 = evaluator.metric(member, "15 years", 2004, 2019);
            metrics.add(evaluated_absolute);
            metrics.add(evaluated_05);
            metrics.add(evaluated_10);
            metrics.add(evaluated_15);
            member.setMetrics(metrics);
            Logger.write("[" + i + "] " + member.toString());
            Logger.write(" - absolute " + evaluated_absolute.toString());
            Logger.write(" - 05 " + evaluated_05.toString());
            Logger.write(" - 10 " + evaluated_10.toString());
            Logger.write(" - 15 " + evaluated_15.toString());
            Logger.write(" ");
        }
        ssd.setStatus(Status.metrics);
        ssd.save();
    }

    public static void evaluateMedians(Request request) throws Exception {
        Logger.setFileName(null);
        Logger.write("Evaluating  medians ");
        SSD ssd = new SSD(request.getSSD_name(), request.getSSD_id(), request.getDegree_id());
        if (!ssd.getStatus().equals(Status.metrics)) { Logger.write("Already available"); return; }
        Logger.write("Calculating information...");
        Logger.setFileName("output/logs/log_medians_evaluation_"  + request.getSSD_name() + ".txt");
        Evaluator evaluator = new Evaluator(ssd);
        List<Metric> medians = new ArrayList<>();
        Metric evaluated_absolute_median_ric = evaluator.median("absolute Ric", 0, "Ricercatore", null);
        Metric evaluated_05_median_ric = evaluator.median("05 years Ric", 1, "Ricercatore", null);
        Metric evaluated_10_median_ric = evaluator.median("10 years Ric", 2, "Ricercatore", null);
        Metric evaluated_15_median_ric = evaluator.median("15 years Ric", 3, "Ricercatore", null);
        Metric evaluated_absolute_median_pa = evaluator.median("absolute PA", 0, "Associato", null);
        Metric evaluated_05_median_pa = evaluator.median("05 years PA", 1, "Associato", null);
        Metric evaluated_10_median_pa = evaluator.median("10 years PA", 2, "Associato", null);
        Metric evaluated_15_median_pa = evaluator.median("15 years PA", 3, "Associato", null);
        Metric evaluated_absolute_median_po = evaluator.median("absolute PO", 0, "Ordinario", null);
        Metric evaluated_05_median_po = evaluator.median("05 years PO", 1, "Ordinario", null);
        Metric evaluated_10_median_po = evaluator.median("10 years PO", 2, "Ordinario", null);
        Metric evaluated_15_median_po = evaluator.median("15 years PO", 3, "Ordinario", null);
        medians.add(evaluated_absolute_median_ric);
        medians.add(evaluated_05_median_ric);
        medians.add(evaluated_10_median_ric);
        medians.add(evaluated_15_median_ric);
        medians.add(evaluated_absolute_median_pa);
        medians.add(evaluated_05_median_pa);
        medians.add(evaluated_10_median_pa);
        medians.add(evaluated_15_median_pa);
        medians.add(evaluated_absolute_median_po);
        medians.add(evaluated_05_median_po);
        medians.add(evaluated_10_median_po);
        medians.add(evaluated_15_median_po);
        Logger.write("Absolute ");
        Logger.write(" - " + evaluated_absolute_median_ric.toString());
        Logger.write(" - " + evaluated_absolute_median_pa.toString());
        Logger.write(" - " + evaluated_absolute_median_po.toString());
        Logger.write("05 year ");
        Logger.write(" - " + evaluated_05_median_ric.toString());
        Logger.write(" - " + evaluated_05_median_pa.toString());
        Logger.write(" - " + evaluated_05_median_po.toString());
        Logger.write("10 year ");
        Logger.write(" - " + evaluated_10_median_ric.toString());
        Logger.write(" - " + evaluated_10_median_pa.toString());
        Logger.write(" - " + evaluated_10_median_po.toString());
        Logger.write("15 years ");
        Logger.write(" - " + evaluated_15_median_ric.toString());
        Logger.write(" - " + evaluated_15_median_pa.toString());
        Logger.write(" - " + evaluated_15_median_po.toString());
        Logger.write(" ");
        ssd.setMedians(medians);
        ssd.setStatus(Status.medians);
        ssd.save();
    }

    public static void evaluateFrequencies(Request request) throws Exception {
        Logger.setFileName(null);
        Logger.write("Evaluating  frequencies ");
        SSD ssd = new SSD(request.getSSD_name(), request.getSSD_id(), request.getDegree_id());
        if (!ssd.getStatus().equals(Status.medians)) { Logger.write("Already available"); return; }
        Logger.write("Calculating information...");
        Logger.setFileName("output/logs/log_frequencies_evaluation_"  + request.getSSD_name() + ".txt");
        Evaluator evaluator = new Evaluator(ssd);
        List<Distribution> frequencies = new ArrayList<>();
        Distribution evaluated_absolute_distribution_ric = evaluator.frequency("Absolute RIC", 0, "Ricercatore", null);
        Distribution evaluated_05_distribution_ric = evaluator.frequency("05 years RIC", 1, "Ricercatore", null);
        Distribution evaluated_10_distribution_ric = evaluator.frequency("10 years RIC", 2, "Ricercatore", null);
        Distribution evaluated_15_distribution_ric = evaluator.frequency("15 years RIC", 3, "Ricercatore", null);
        Distribution evaluated_absolute_distribution_pa = evaluator.frequency("Absolute PA", 0, "Associato", null);
        Distribution evaluated_05_distribution_pa = evaluator.frequency("05 years PA", 1, "Associato", null);
        Distribution evaluated_10_distribution_pa = evaluator.frequency("10 years PA", 2, "Associato", null);
        Distribution evaluated_15_distribution_pa = evaluator.frequency("15 years PA", 3, "Associato", null);
        Distribution evaluated_absolute_distribution_po = evaluator.frequency("Absolute PO", 0, "Ordinario", null);
        Distribution evaluated_05_distribution_po = evaluator.frequency("05 years PO", 1, "Ordinario", null);
        Distribution evaluated_10_distribution_po = evaluator.frequency("10 years PO", 2, "Ordinario", null);
        Distribution evaluated_15_distribution_po = evaluator.frequency("15 years PO", 3, "Ordinario", null);
        frequencies.add(evaluated_absolute_distribution_ric);
        frequencies.add(evaluated_05_distribution_ric);
        frequencies.add(evaluated_10_distribution_ric);
        frequencies.add(evaluated_15_distribution_ric);
        frequencies.add(evaluated_absolute_distribution_pa);
        frequencies.add(evaluated_05_distribution_pa);
        frequencies.add(evaluated_10_distribution_pa);
        frequencies.add(evaluated_15_distribution_pa);
        frequencies.add(evaluated_absolute_distribution_po);
        frequencies.add(evaluated_05_distribution_po);
        frequencies.add(evaluated_10_distribution_po);
        frequencies.add(evaluated_15_distribution_po);
        Logger.write("Absolute ");
        Logger.write(" - " + evaluated_absolute_distribution_ric.toString());
        Logger.write(" - " + evaluated_absolute_distribution_pa.toString());
        Logger.write(" - " + evaluated_absolute_distribution_po.toString());
        Logger.write("05 years ");
        Logger.write(" - " + evaluated_05_distribution_ric.toString());
        Logger.write(" - " + evaluated_05_distribution_pa.toString());
        Logger.write(" - " + evaluated_05_distribution_po.toString());
        Logger.write("10 years ");
        Logger.write(" - " + evaluated_10_distribution_ric.toString());
        Logger.write(" - " + evaluated_10_distribution_pa.toString());
        Logger.write(" - " + evaluated_10_distribution_po.toString());
        Logger.write("15 years ");
        Logger.write(" - " + evaluated_15_distribution_ric.toString());
        Logger.write(" - " + evaluated_15_distribution_pa.toString());
        Logger.write(" - " + evaluated_15_distribution_po.toString());
        Logger.write(" ");
        ssd.setFrequencies(frequencies);
        ssd.setStatus(Status.frequencies);
        ssd.save();
    }

    public static void evaluatePercentile(Request request) throws Exception {
        Logger.setFileName(null);
        Logger.write("Evaluating  percentile ");
        SSD ssd = new SSD(request.getSSD_name(), request.getSSD_id(), request.getDegree_id());
        if (!ssd.getStatus().equals(Status.frequencies)) { Logger.write("Already available"); return; }
        Logger.write("Calculating information...");
        Logger.setFileName("output/logs/log_percentile_evaluation_"  + request.getSSD_name() + ".txt");
        Evaluator evaluator = new Evaluator(ssd);
        List<Distribution> percentiles = new ArrayList<>();
        Distribution evaluated_absolute_percentile_ric = evaluator.percentile("Absolute RIC", 0, "Ricercatore", null);
        Distribution evaluated_05_percentile_ric = evaluator.percentile("05 years RIC", 1, "Ricercatore", null);
        Distribution evaluated_10_percentile_ric = evaluator.percentile("10 years RIC", 2, "Ricercatore", null);
        Distribution evaluated_15_percentile_ric = evaluator.percentile("15 years RIC", 3, "Ricercatore", null);
        Distribution evaluated_absolute_percentile_pa = evaluator.percentile("Absolute PA", 0, "Associato", null);
        Distribution evaluated_05_percentile_pa = evaluator.percentile("05 years PA", 1, "Associato", null);
        Distribution evaluated_10_percentile_pa = evaluator.percentile("10 years PA", 2, "Associato", null);
        Distribution evaluated_15_percentile_pa = evaluator.percentile("15 years PA", 3, "Associato", null);
        Distribution evaluated_absolute_percentile_po = evaluator.percentile("Absolute PO", 0, "Ordinario", null);
        Distribution evaluated_05_percentile_po = evaluator.percentile("05 years PO", 1, "Ordinario", null);
        Distribution evaluated_10_percentile_po = evaluator.percentile("10 years PO", 2, "Ordinario", null);
        Distribution evaluated_15_percentile_po = evaluator.percentile("15 years PO", 3, "Ordinario", null);
        percentiles.add(evaluated_absolute_percentile_ric);
        percentiles.add(evaluated_05_percentile_ric);
        percentiles.add(evaluated_10_percentile_ric);
        percentiles.add(evaluated_15_percentile_ric);
        percentiles.add(evaluated_absolute_percentile_pa);
        percentiles.add(evaluated_05_percentile_pa);
        percentiles.add(evaluated_10_percentile_pa);
        percentiles.add(evaluated_15_percentile_pa);
        percentiles.add(evaluated_absolute_percentile_po);
        percentiles.add(evaluated_05_percentile_po);
        percentiles.add(evaluated_10_percentile_po);
        percentiles.add(evaluated_15_percentile_po);
        Logger.write("Absolute ");
        Logger.write(" - " + evaluated_absolute_percentile_ric.toString());
        Logger.write(" - " + evaluated_absolute_percentile_pa.toString());
        Logger.write(" - " + evaluated_absolute_percentile_po.toString());
        Logger.write("05 years ");
        Logger.write(" - " + evaluated_05_percentile_ric.toString());
        Logger.write(" - " + evaluated_05_percentile_pa.toString());
        Logger.write(" - " + evaluated_05_percentile_po.toString());
        Logger.write("10 years ");
        Logger.write(" - " + evaluated_10_percentile_ric.toString());
        Logger.write(" - " + evaluated_10_percentile_pa.toString());
        Logger.write(" - " + evaluated_10_percentile_po.toString());
        Logger.write("15 years ");
        Logger.write(" - " + evaluated_15_percentile_ric.toString());
        Logger.write(" - " + evaluated_15_percentile_pa.toString());
        Logger.write(" - " + evaluated_15_percentile_po.toString());
        Logger.write(" ");
        ssd.setPercentiles(percentiles);
        ssd.setStatus(Status.percentiles);
        ssd.save();
    }
    
    public static void exportJSON_CSV_Charts(Request request) throws IOException, Exception {
        Logger.setFileName(null);
        Logger.write("Exporting to JSON, CSV and Charts");
        SSD ssd = new SSD(request.getSSD_name(), request.getSSD_id(), request.getDegree_id());
        if (!ssd.getStatus().equals(Status.percentiles)) { Logger.write("Already available"); return; }
        Logger.write(" - to JSON...");
        Utils.toJSONFile(ssd);
        Logger.write(" - to CSV...");
        Utils.toCSVFile(ssd);
        Logger.write(" - to Charts...");
        Utils.toChart(ssd);
        ssd.setStatus(Status.complete);
        ssd.save();
        Logger.write("Complete");
    }
}