package elios.impactcrawler;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;

public class Unit implements Aggregate {
    private String name;
    private Status status = Status.empty;
    private List<Member> members;
    private List<Metric> medians;
    private List<Distribution> frequencies;
    private List<Distribution> percentiles;

    public Unit(String name) { 
        this.name = name;
        this.load();
    }

    public String getName() { return name; }
    public void setMembers(List<Member> members) { this.members = members; }
    public List<Member> getMembers() { return members; }
    public List<Metric> getMedians() { return medians; }
    public void setMedians(List<Metric> medians) { this.medians = medians; }
    public List<Distribution> getFrequencies() { return frequencies; }
    public void setFrequencies(List<Distribution> frequencies) { this.frequencies = frequencies; }
    public List<Distribution> getPercentiles() { return percentiles; }
    public void setPercentiles(List<Distribution> percentiles) { this.percentiles = percentiles; } 
    public void setStatus(Status status) { this.status = status; }
    public Status getStatus() { return status; }

    public void load() {
        Unit ssd = null;
        InputStream file = null;
        try {
           if (Files.exists(Paths.get("output/commmission-" + name + ".ser"))) {
                file = new FileInputStream("output/commmission-" + name + ".ser");
                InputStream buffer = new BufferedInputStream(file);
                ObjectInput input = new ObjectInputStream (buffer);
                name = (String)input.readObject();
                status = (Status)input.readObject();
                members = (List<Member>)input.readObject();
                frequencies = (List<Distribution>)input.readObject();
                percentiles = (List<Distribution>)input.readObject();
                medians = (List<Metric>)input.readObject();
           }
        } 
        catch (Exception ex) { } 
        finally {
            try {
                if (Files.exists(Paths.get("output/commmission-" + name + ".ser"))) 
                    file.close();
            }
            catch (IOException ex) {
                java.util.logging.Logger.getLogger(Unit.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void save() {
        try {
            OutputStream file = new FileOutputStream("output/commmission-" + name + ".ser");
            OutputStream buffer = new BufferedOutputStream(file);
            ObjectOutput output = new ObjectOutputStream(buffer);
            output.writeObject(this.getName());
            output.writeObject(this.getStatus());
            output.writeObject(this.getMembers());
            output.writeObject(this.getFrequencies());
            output.writeObject(this.getPercentiles());
            output.writeObject(this.getMedians());
            output.close();
        } 
        catch (IOException ex) { java.util.logging.Logger.getLogger(Unit.class.getName()).log(Level.SEVERE, null, ex); }
    }
}
