package elios.impactcrawler;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.List;
import java.util.logging.Level;

public class Member implements Serializable {
    private final String name;
    private final String surname;
    private final String university;
    private final String role;
    
    private String scopusid = null;
    
    private String ge = null;
    private String area = null;
    private String unit = null;
    
    private List<Metric> metrics = null;
    private List<Paper> papers = null;
    private List<Trend> trends = null;

    public Member(String name, String surname, String role, String university) {
        this.name = name;
        this.surname = surname;
        this.university = university;
        this.role = role;
        Logger.write(" - " + surname +  " " + name + " (" + role + ", " + university + ")");
    }
    
    public Member(String name, String surname, String role, String university, String scopusid) {
        this.name = name;
        this.surname = surname;
        this.university = university;
        this.role = role;
        this.scopusid = scopusid;
        Logger.write(" - " + surname +  " " + name + " (" + role + ", " + university + ")");
    }

    public void setTrends(List<Trend> trends) { this.trends = trends; }
    public List<Trend> getTrends() { return trends; }
    public void setMetrics(List<Metric> metrics) { this.metrics = metrics; }
    public List<Metric> getMetrics() { return metrics; }
    public void setPapers(List<Paper> papers) { this.papers = papers; }
    public List<Paper> getPapers() { return papers; }
    public String getScopusid() { return scopusid; } 
    public void setScopusid(String scopusid) { this.scopusid = scopusid; }
    public String getName() { return name; }
    public String getSurname() { return surname; }
    public String getUniversity() { return university; }
    public String getRole() { return role; }
    public void setGe(String ge) { this.ge = ge; }
    public String getGe() { return ge; }
    public void setArea(String area) { this.area = area; }
    public String getArea() { return area; }
    public String getUnit() { return unit; }
    public void setUnit(String unit) { this.unit = unit; }
    
    @Override
    public String toString()  {
        String description = "Member{";
        Field[] fields = Member.class.getDeclaredFields();
        for (Field field : fields)  {
            if(field.getGenericType() == String.class) {
                try {
                    if(field.get(this) != null) description += " " + field.getName() + ":" + (String)field.get(this) + ";";
                } 
                catch (IllegalArgumentException ex) { java.util.logging.Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex); } 
                catch (IllegalAccessException ex) { java.util.logging.Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex); }
            }
        }
        description += "}";
        return description;
    }   
}
