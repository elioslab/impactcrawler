package elios.impactcrawler;

import java.util.ArrayList;
import java.util.List;
import org.json.*;

public class Miur {
    private final String url = "http://dati.ustat.miur.it/api/3/action/datastore_search";
    
    private int get(String degree, String resource) throws Exception {
        HTTP http = new HTTP();
        int total = 0;
        String answer = http.sendGet(url + "?resource_id=" + resource + "&q=" + degree);
        JSONObject obj1 = new JSONObject(answer);
        JSONObject obj2 = obj1.getJSONObject("result");
        JSONArray list = obj2.getJSONArray("records");
        for(int i=0; i<list.length(); i++)
            total += list.getJSONObject(i).getInt("LAUREATI_TOTALE");
        return total;
    }
    
    private int get2015(String degree, String resource) throws Exception {
        HTTP http = new HTTP();
        int total = 0;
        String answer = http.sendGet(url + "?resource_id=" + resource + "&q=" + degree);
        JSONObject obj1 = new JSONObject(answer);
        JSONObject obj2 = obj1.getJSONObject("result");
        JSONArray list = obj2.getJSONArray("records");
        for(int i=0; i<list.length(); i++){
            total += list.getJSONObject(i).getInt("LAUREATI_MASCHI");
            total += list.getJSONObject(i).getInt("LAUREATI_FEMMINE");
        }
        return total;
    }
            
    public List<Graduates> getGraduatesNumber(String degree) throws Exception {
        List<Graduates> list = new ArrayList<>();
        list.add(new Graduates(2017, this.get(degree, "cd23705b-7192-4735-85b2-f79a96af6557")));
        list.add(new Graduates(2016, this.get(degree, "efe0c5d9-4204-427a-b872-71e68c794da9")));
        list.add(new Graduates(2015, this.get2015(degree, "c98288dc-1868-4256-aefb-c4c98c4bc82f")));
        return list;
    }
}
