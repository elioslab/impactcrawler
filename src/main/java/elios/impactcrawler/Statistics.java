package elios.impactcrawler;

public class Statistics 
{
    int[] data;
    int size;   

    public Statistics(int[] data) {
        this.data = data;
        size = data.length;
    }   

    public double getMean() {
        double sum = 0.0;
        for(double a : data) sum += a;
        return sum/size;
    }

    public double getVariance() {
        double mean = getMean();
        double temp = 0;
        for(double a :data) temp += (mean-a)*(mean-a);
        return temp/size;
    }

    public double getStdDev() { return Math.sqrt(getVariance()); }
    
    public int[] getFrequencies(int segment) {
        int limit = (int)(getMean() + 1.5 * this.getStdDev());
        int start = 0;
        int[] counts   = new int[(limit-start)/segment+1];
        for (int i = 0; i < data.length; i++) {
            int element = data[i];
            int index = (int) ((element - start) / segment);
            if(index >= counts.length) index = counts.length-1;
            counts[index]++;
        }
        return counts;
    }
    
    public int[] getPercentile(int segment) {
        int[] distribution = getFrequencies(segment);
        float[] temp = new float[distribution.length];
        int[] percentile = new int[distribution.length];
        int total = 0;
        for(int i=0; i<distribution.length; i++) total += distribution[i];
        temp[0] = (((float)distribution[0] / total) * 100);
        for(int i=1; i<distribution.length; i++) {
            float value = (((float)distribution[i] / total) * 100);
            temp[i] = temp[i-1] + value;
        }
        for(int i=0; i<temp.length; i++) percentile[i] = (int)temp[i];
        return percentile;
    }
}
