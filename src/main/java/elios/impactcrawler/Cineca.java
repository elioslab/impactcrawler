package elios.impactcrawler;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

public class Cineca {
    private final String url = "https://cercauniversita.cineca.it/php5/docenti/vis_docenti.php";
    
    public List<Member> getMembers(String ssd) throws Exception {
        List<Member> members = new ArrayList<>();
        
        List<NameValuePair> body = new ArrayList<>(2);
        body.add(new BasicNameValuePair("area", "0000"));
        body.add(new BasicNameValuePair("cognome", ""));
        body.add(new BasicNameValuePair("conferma", "2"));
        body.add(new BasicNameValuePair("facolta", "00"));
        body.add(new BasicNameValuePair("genere", "A"));
        body.add(new BasicNameValuePair("macro", "0000"));
        body.add(new BasicNameValuePair("nome", ""));
        body.add(new BasicNameValuePair("qualifica", "**"));
        body.add(new BasicNameValuePair("radiogroup", "P"));
        body.add(new BasicNameValuePair("settore", ssd));
        body.add(new BasicNameValuePair("settorec", "0000"));
        body.add(new BasicNameValuePair("situazione_al", "0"));
        body.add(new BasicNameValuePair("universita", "00"));
        body.add(new BasicNameValuePair("testuale", "1"));
        
        HTTP http = new HTTP();
        
        String[] list = filterMembers(http.sendPost(url, body));
        
        Integer number = list.length;
        for(int i=1; i<number; i++)
            members.add(this.crateMember(list[i]));

        return members;
    }
    
    private Member crateMember(String description) {
        String[] list = description.split(";");
        
        String name = list[2];
        String surname = list[1];
        String university = list[4];
        String role = list[0];
        
        return new Member(name, surname, role, university);
    }
    
    private String[] filterMembers(String raw) {
        String[] list = raw.split("<span");
        
        for(int i=1; i<list.length; i++) {
            list[i] = list[i].replaceFirst("&nbsp;", ";");
            list[i] = list[i].replaceAll("</td><td>", ";");
            list[i] = list[i].replaceAll("class=\"pac\">", "");
            list[i] = list[i].replaceAll("class=\"po\">", "");
            list[i] = list[i].replaceAll("class=\"po8\">", ""); 
            list[i] = list[i].replaceAll("class=\"ruc\">", "");
            list[i] = list[i].replaceAll("class=\"pa8\">", "");
            list[i] = list[i].replaceAll("class=\"pari\">", "");
            list[i] = list[i].replaceAll("class=\"rd115\">", "");
            list[i] = list[i].replaceAll("class=\"od\">", "");
            list[i] = list[i].replaceAll("class=\"rd118\">", "");
            list[i] = list[i].replaceAll("class=\"rd107\">", "");
            list[i] = list[i].replaceAll("class=\"st\">", "");
            list[i] = list[i].replaceAll("</td>  <tr>", "");
            list[i] = list[i].replaceAll("</td><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/></table>", "");
            list[i] = list[i].replaceAll("class=\"rd124\">", "");
            list[i] = list[i].replaceAll("</span>", "");
            list[i] = list[i].replaceAll("</td><tr <td>", "");
            list[i] = list[i].replaceAll("<td>", "");
        }
        return list;
    }
}
