package elios.impactcrawler.phdcommission;

import elios.impactcrawler.Cineca;
import elios.impactcrawler.Unit;
import elios.impactcrawler.Distribution;
import elios.impactcrawler.Evaluator;
import elios.impactcrawler.Logger;
import elios.impactcrawler.Member;
import elios.impactcrawler.Metric;
import elios.impactcrawler.Miur;
import elios.impactcrawler.Paper;
import elios.impactcrawler.Request;
import elios.impactcrawler.SSD;
import elios.impactcrawler.Scopus;
import elios.impactcrawler.Status;
import elios.impactcrawler.Utils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class phdcommission {
    
    public static void main(String[] args) throws Exception {
        
        Logger.setVerbose(true);
                
        File dir = new File("output"); dir.mkdir();
        dir = new File("output/images"); dir.mkdir();
        dir = new File("output/frequencies"); dir.mkdir();
        dir = new File("output/percentiles"); dir.mkdir();
        dir = new File("output/logs"); dir.mkdir();
        dir = new File("output/graduates"); dir.mkdir();

        Unit commission = new Unit("PhdSTIET");
        List<Member> members = new ArrayList<>();        
        members.add(new Member("Riccardo", "Berta", "Associato", "UNIGE", "6701751695"));
        members.add(new Member("Mario", "Marchese", "Ordinario", "UNIGE", "7004398165"));
        members.add(new Member("Federico", "Delfino", "Ordinario", "UNIGE", "6603766602"));
        members.add(new Member("Franco", "Davoli", "Ordinario", "UNIGE", "12239775200"));
        members.add(new Member("Silvana", "Dellepiane", "Associato", "UNIGE", "7004291247"));
        members.add(new Member("Ermanno", "Di Zitti", "Associato", "UNIGE", "6603663623"));
        members.add(new Member("Fabio", "Lavagetto", "Ordinario", "UNIGE", "7004136693"));
        members.add(new Member("Lucio", "Marcenaro", "Ricercatore", "UNIGE", "6603377664"));
        members.add(new Member("Matteo", "Pastorino", "Ordinario", "UNIGE", "7004880389"));
        members.add(new Member("Andrea", "Randazzo", "Associato", "UNIGE", "7006280562"));
        members.add(new Member("Bruno", "Serpico", "Ordinario", "UNIGE", "7005306316"));
        members.add(new Member("Maurizio", "Valle", "Associato", "UNIGE", "57197744325"));
        members.add(new Member("Rodolfo", "Zunino", "Associato", "UNIGE", "7006338311"));
        
        commission.setMembers(members);
        commission.setStatus(Status.scopusid);
        commission.save();
        
        getPapers(commission.getName());
        evaluateMetrics(commission.getName());
        exportJSON_CSV_Charts(commission.getName());
    }
    
    public static void getPapers(String commission_name) throws Exception {
        Logger.setFileName(null);
        Logger.write("Searching for papers from Scopus");
        Scopus scopus = new Scopus();
        Unit commission = new Unit(commission_name);
        if (!commission.getStatus().equals(Status.scopusid)) { Logger.write("Already available"); return; }
        Logger.write("Crawling information...");
        Logger.setFileName("output/logs/log_papers_scopus_"  + commission.getName() + ".txt");
        List<Member> list = commission.getMembers();
        for (int i = 0; i < list.size(); i++) {
            Member member = list.get(i);
            Logger.write("[" + i + "] " + member.toString());
            if (member.getScopusid() != null) {
                List<Paper> papers = scopus.getPapers(member.getScopusid());
                member.setPapers(papers);
            }
        }
        commission.setStatus(Status.scopuspaper);
        commission.save();
    }

    public static void evaluateMetrics(String commission_name) throws Exception {
        Logger.setFileName(null);
        Logger.write("Evaluating  metric");
        Unit commission = new Unit(commission_name);
        if (!commission.getStatus().equals(Status.scopuspaper)) { Logger.write("Already available"); return; }
        Logger.write("Calculating information...");
        Logger.setFileName("output/logs/log_metrics_evaluation_"  + commission.getName() + ".txt");
        List<Member> list = commission.getMembers();
        for (int i = 0; i < list.size(); i++) {
            List<Metric> metrics = new ArrayList<>();
            Member member = list.get(i);
            Evaluator evaluator = new Evaluator(commission);
            Metric evaluated_absolute = evaluator.metric(member, "absolute", 1900, 2019);
            Metric evaluated_05 = evaluator.metric(member, "05 years", 2015, 2018);
            Metric evaluated_10 = evaluator.metric(member, "10 years", 2008, 2018);
            Metric evaluated_15 = evaluator.metric(member, "15 years", 2004, 2018);
            metrics.add(evaluated_absolute);
            metrics.add(evaluated_05);
            metrics.add(evaluated_10);
            metrics.add(evaluated_15);
            member.setMetrics(metrics);
            Logger.write("[" + i + "] " + member.toString());
            Logger.write(" - absolute " + evaluated_absolute.toString());
            Logger.write(" - 05 " + evaluated_05.toString());
            Logger.write(" - 10 " + evaluated_10.toString());
            Logger.write(" - 15 " + evaluated_15.toString());
            Logger.write(" ");
        }
        commission.setStatus(Status.metrics);
        commission.save();
    }

       
    public static void exportJSON_CSV_Charts(String commission_name) throws IOException, Exception {
        Logger.setFileName(null);
        Logger.write("Exporting to JSON, CSV and Charts");
        Unit commission = new Unit(commission_name);
        if (!commission.getStatus().equals(Status.metrics)) { Logger.write("Already available"); return; }
        Logger.write(" - to JSON...");
        Utils.toJSONFile(commission);
        Logger.write(" - to CSV...");
        Utils.toCSVFileMembers(commission);
        commission.setStatus(Status.complete);
        commission.save();
        Logger.write("Complete");
    }
}