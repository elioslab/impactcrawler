package elios.impactcrawler;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Scopus {
    //private final String api_key = "05c0133a54a3c916f49fc731b32bfdb0";
    //private final String api_key = "b3a71de2bde04544495881ed9d2f9c5b";
    //private final String api_key = "0627ae4f375bbba2f9ecd8b049ba35db";
    private final String api_key = "82d06d659d73e5ba459e6dcf4a5c7e1c";
    private final String url = "https://api.elsevier.com/content";
    
    public String getScopusID(String name, String surname, String affiliation) throws Exception {
        String firstName = name.split(" ")[0];
        String query = "/search/author?query=authlast(" + URLEncoder.encode(surname, "UTF-8") + ")%20and%20authfirst(" + URLEncoder.encode(firstName, "UTF-8") +")&apiKey=" + api_key;
        HTTP http = new HTTP();
        String result = http.sendGet(url + query);
        return filterID(result, affiliation);
    }
    
    private String filterSubjectArea(JSONArray areas) {
        String result = "";
        for(int i=0; i<areas.length(); i++) {
            try { 
                String value = areas.getJSONObject(i).getString("@abbrev"); 
                result += value + " ";
            } 
            catch (JSONException ex) { }
        }
        return result;
    }
    
    private double evaluate(String cineca_affiliation, String cineca_areas, String scopus_affiliation, String scopus_areas) {
        double affiliation = 0.0f;
        if(cineca_affiliation != null && scopus_affiliation != null)
            affiliation = Utils.similarity(cineca_affiliation, scopus_affiliation);
        double areas = 0.0f;
        if(cineca_areas != null && scopus_areas != null)
            areas = Utils.similarity(cineca_areas, scopus_areas);
        return affiliation + areas/5.0f;
    }
    
    private String filterID(String raw, String affiliation) throws JSONException, IOException {
        double similitude = 0.0f;
        int index = 0;
        String eid = null;
        JSONObject obj = new JSONObject(raw);
        JSONArray list = obj.getJSONObject("search-results").getJSONArray("entry");
        Logger.write("Found from Scopus:");
        for(int i=0; i<list.length(); i++) {
            String affiliationTemp = null;
            String subjectAreaTemp = null;
            String eidTemp = null;
            JSONObject temp = list.getJSONObject(i);
            try { affiliationTemp = temp.getJSONObject("affiliation-current").getString("affiliation-name"); } catch(Exception ex) {}
            try { subjectAreaTemp = filterSubjectArea(temp.getJSONArray("subject-area")); }  catch(Exception ex) {}
            try { eidTemp = temp.getString("eid"); } catch(Exception ex) {}
            double similitudeTemp = evaluate(affiliation, "ENGI COMP PHYS MATH MATE", affiliationTemp, subjectAreaTemp);
            if (similitudeTemp > similitude) {
                similitude = similitudeTemp;
                eid = eidTemp;
                index = i;
            } 
            Logger.write(" - [" + eidTemp + "] " + affiliationTemp + " (" + subjectAreaTemp + ")" + " -> similitude = " + similitudeTemp);
        }
        Logger.write("Selected from scopus: " + eid);
        return eid;
    }

    public String getAuthorAbsoluteMetrics(String scopusid) throws Exception {
        String query = "/author/author_id/" + scopusid + "?apiKey=" + api_key + "&view=METRICS&httpAccept=application/json";
        HTTP http = new HTTP();
        String result = http.sendGet(url + query);
        return result;
    }

    public Integer getCitationCount(String metadata) throws JSONException {
        JSONObject temp = new JSONObject(metadata);
        Integer citations = -1;
        try { citations = temp.getJSONArray("author-retrieval-response").getJSONObject(0).getJSONObject("coredata").getInt("citation-count"); } catch(Exception ex) {}
        return citations;
    }

    public Integer getHindex(String metadata) throws JSONException {
        Integer hindex = -1;
        JSONObject temp = new JSONObject(metadata);
        try { hindex = temp.getJSONArray("author-retrieval-response").getJSONObject(0).getInt("h-index"); } catch(Exception ex) {}
        return hindex;
    }

    public Integer getDocumentCount(String metadata) throws JSONException {
        JSONObject temp = new JSONObject(metadata);
        Integer documents = -1;
        try { documents =temp.getJSONArray("author-retrieval-response").getJSONObject(0).getJSONObject("coredata").getInt("document-count"); } catch(Exception ex) {}
        return documents;
    }
    
    public List<Paper> getCitations(Paper paper) throws Exception {
        List<Paper> papers = new ArrayList<>();
        String query = null;
        String result = null;
        HTTP http = null;
        JSONObject temp = null;
        
        while(temp == null) {
            try {
                query = "/abstract/citations?query=scopus_id(" + paper.getId() + ")&apiKey=" + api_key;
                //query = "/abstract/scopus_id/" + paperid + "?apiKey=" + api_key + "&httpAccept=Application/json";
                //query = "/search/scopus?query=refid(" + paper.getEid() + ")&apiKey=" + api_key + "&httpAccept=Application/json";
                
                http = new HTTP();
                result = http.sendGet(url + query);
                temp = new JSONObject(result); //.getJSONObject("bibliography");
            }
            catch(Exception e) {
                System.out.println("Scopus API fail");
            }
        }
        
        /*
        for(int i=0; i<ids.length(); i++) {
            JSONObject paperTemp = ids.getJSONObject(i);
            String id = "unknown";
            String title = "unknown";
            String year = "unknown";
            String type = "unknown";
            String subtype = "unknown";
            String citations = "0";

            try { id = paperTemp.getString("dc:identifier").replace("SCOPUS_ID:", ""); } catch (Exception e) { Logger.write("Warning, paper identifier not found"); };
            try { title = paperTemp.getString("dc:title"); } catch (Exception e) { Logger.write("Warning, paper title not found"); }
            try { year = paperTemp.getString("prism:coverDisplayDate"); } catch (Exception e) { Logger.write("Warning, paper year not found"); }
            try { type = paperTemp.getString("prism:aggregationType"); } catch (Exception e) { Logger.write("Warning, paper type not found"); }
            try { citations = paperTemp.getString("citedby-count"); } catch (Exception e) { Logger.write("Warning, paper citations not found"); }
            try { subtype = paperTemp.getString("subtypeDescription"); } catch (Exception e) { Logger.write("Warning, paper subtype not found"); }
            Paper paper = new Paper(id, title, year, type, subtype, citations);


            papers.add(paper);
            Logger.write("      - Referered in: " + paper.toString());
        }
        Logger.write("");
        */
        
        return papers;
    }
    
    public List<Paper> getPapers(String scopusid) throws Exception {
        List<Paper> papers = new ArrayList<>();
        String query = null;
        String result = null;
        HTTP http = null;
        JSONObject temp = null;
        while(temp == null) {
            try {
                query = "/search/scopus/?query=AU-ID(" + scopusid.replace("9-s2.0-", "") + ")&apiKey=" + api_key;
                http = new HTTP();
                result = http.sendGet(url + query);
                temp = new JSONObject(result).getJSONObject("search-results");
            }
            catch(Exception e) {
                System.out.println("Scopus API fail");
            }
        }
        Integer total = Integer.parseInt(temp.getString("opensearch:totalResults"));
        Integer loops = total / 25 + 1;
        Integer index = 0;
        Logger.write(" - Total: " + total);
        
        for(int counter=0; counter<loops; counter++){
            query = "/search/scopus/?query=AU-ID(" + scopusid.replace("9-s2.0-", "") + ")&apiKey=" + api_key + "&start=" + 25*counter;
            temp = null;
            while(temp == null){
                result = http.sendGet(url + query);
                //for(int j=0; j<10;j++) result = result.replaceFirst("link", "L_new_ink" + j);
                //for(int j=0; j<10;j++) result = result.replaceFirst("subtype", "sub_new_type" + j);
                try {
                    temp = new JSONObject(result).getJSONObject("search-results");
                }
                catch(Exception e) {
                    System.out.println("Scopus API fail");
                    break;
                }
            }     
            
            JSONArray ids = null;
            try { ids = temp.getJSONArray("entry"); } catch(Exception e) { Logger.write("Warning, non papers founs"); };
            if(ids != null) {
                for(int i=0; i<ids.length(); i++) {
                    JSONObject paperTemp = ids.getJSONObject(i);
                    String id = "unknown";
                    String eid = "unknown";
                    String doi = "unknown";
                    String title = "unknown";
                    String from = "unknown";
                    String year = "unknown";
                    String type = "unknown";
                    String subtype = "unknown";
                    String citations = "0";
             
                    try { id = paperTemp.getString("dc:identifier").replace("SCOPUS_ID:", ""); } catch (Exception e) { Logger.write("Warning, paper identifier not found"); };
                    try { eid = paperTemp.getString("eid"); } catch (Exception e) { Logger.write("Warning, eid title not found"); }
                    try { doi = paperTemp.getString("prism:doi"); } catch (Exception e) { Logger.write("Warning, doi title not found"); }
                    try { title = paperTemp.getString("dc:title"); } catch (Exception e) { Logger.write("Warning, paper title not found"); }
                    try { from = paperTemp.getString("prism:publicationName"); } catch (Exception e) { Logger.write("Warning, paper from not found"); }
                    try { year = paperTemp.getString("prism:coverDisplayDate"); } catch (Exception e) { Logger.write("Warning, paper year not found"); }
                    try { type = paperTemp.getString("prism:aggregationType"); } catch (Exception e) { Logger.write("Warning, paper type not found"); }
                    try { citations = paperTemp.getString("citedby-count"); } catch (Exception e) { Logger.write("Warning, paper citations not found"); }
                    try { subtype = paperTemp.getString("subtypeDescription"); } catch (Exception e) { Logger.write("Warning, paper subtype not found"); }
                    
                    //try { subtype = paperTemp.getString("subtypeDescription"); } catch (Exception e1) { 
                    //try { subtype = paperTemp.getString("sub_new_type1Description"); }  catch (Exception e2)  { 
                    //try { subtype = paperTemp.getString("sub_new_type2Description"); }  catch (Exception e3)  { 
                    //try { subtype = paperTemp.getString("sub_new_type3Description"); }  catch (Exception e4)  { 
                    //try { subtype = paperTemp.getString("sub_new_type4Description"); }  catch (Exception e5)  { 
                    //try { subtype = paperTemp.getString("sub_new_type5Description"); }  catch (Exception e6)  { 
                    //try { subtype = paperTemp.getString("sub_new_type6Description"); }  catch (Exception e7)  { 
                    //try { subtype = paperTemp.getString("sub_new_type7Description"); }  catch (Exception e8)  { 
                    //try { subtype = paperTemp.getString("sub_new_type8Description"); }  catch (Exception e9)  { 
                    //try { subtype = paperTemp.getString("sub_new_type9Description"); }  catch (Exception e10) { 
                    //try { subtype = paperTemp.getString("sub_new_type10Description"); } catch (Exception e11) { 
                    //try { subtype = paperTemp.getString("sub_new_type11Description"); } catch (Exception e12) { 
                    //try { subtype = paperTemp.getString("sub_new_type12Description"); } catch (Exception e13) { Logger.write("Warning, paper subtype not found " + paperTemp.toString() );
                    //};};};};};};};};};};};};};         
                                                          
                    Paper paper = new Paper(id, eid, doi, title, from, year, type, subtype, citations);
                    paper.setCitations(this.getCitations(paper));
                    
                    papers.add(paper);
                    Logger.write("  - [" + index + "] " + paper.toString());
                    index++;
                }
            }
        }
        Logger.write("");
        return papers;
    }
}
