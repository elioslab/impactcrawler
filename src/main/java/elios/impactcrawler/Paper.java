package elios.impactcrawler;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class Paper implements Serializable {
    private final String id;
    private final String eid;
    private final String doi;
    private final String title;
    private final String from;
    private final String citations_number;
    private final String year;
    private final String type;
    private final String subtype;
    private List<Paper> citations;

    public Paper(String id, String eid, String doi, String title, String from, String year, String type, String subtype, String citations_number) { 
        this.id = id; 
        this.eid = eid;
        this.doi = doi;
        this.title = title;
        this.from = from;
        this.type = type;
        this.subtype = subtype;
        this.year = year;
        this.citations_number = citations_number;
        this.citations = new ArrayList<>();
    }
    
    public String getId() { return id; }
    public String getEid() { return eid; }
    public String getDoi() { return doi; }
    public String getTitle() { return title; }
    public String getFrom() { return from; }
    public String getCitationsNumber() { return citations_number; }
    public String getYear() { return year; }
    public String getType() { return type; }
    public String getSubtype() { return subtype; }
    public List<Paper> getCitations() { return citations; }
    
    public void setCitations(List<Paper> citations) { this.citations = citations; }
    
    @Override
    public String toString()  {
        String description = "Paper{";
        Field[] fields = Paper.class.getDeclaredFields();
        for (Field field : fields) {
            if(field.getGenericType() == String.class) {
                try {
                    if(field.get(this) != null) description += " " + field.getName() + ":" + (String)field.get(this) + ";";
                } 
                catch (IllegalArgumentException ex) { java.util.logging.Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex); } 
                catch (IllegalAccessException ex) { java.util.logging.Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex); }
            }
        }
        description += "}";
        return description;
    }
}
