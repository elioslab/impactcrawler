package elios.impactcrawler;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class Trend implements Serializable
{
    private final String name;
    private final Integer startYear;
    private final Integer stop_year;
    private final Integer lenght;
    private List<Metric> metrics;

    public Trend(String name, Integer startYear, Integer stop_year, Integer lenght) {
        this.name = name;
        this.startYear = startYear;
        this.stop_year = stop_year;
        this.lenght = lenght;
        this.metrics = new ArrayList<>();
    }

    public void setMetrics(List<Metric> metrics) { this.metrics = metrics; }
    public String getName() { return name; }
    public Integer getStartYear() { return startYear; }
    public Integer getStopYear() { return stop_year; }
    public Integer getLenght() { return lenght; }
    public List<Metric> getMetrics() { return metrics; }
    
    @Override
    public String toString()  {
        String description = "Trend{";
        Field[] fields = Trend.class.getDeclaredFields();
        for (Field field : fields) {
            try {
                if(field.get(this) != null) {
                    if(field.getGenericType() == String.class) description += " " + field.getName() + ":" + (String)field.get(this) + ";";
                    else description += " " + field.getName() + ":" + (Integer)field.get(this) + ";";
                }
            } 
            catch (IllegalArgumentException ex) { java.util.logging.Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex); } 
            catch (IllegalAccessException ex) { java.util.logging.Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex); }
        }
        description += "}";
        return description;
    }
}
