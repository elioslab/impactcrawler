package elios.impactcrawler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.codehaus.jackson.map.ObjectMapper;

public class Utils 
{
    public static Date getDate() { return new Date(); }
    
    public static double similarity(String s1, String s2) {
        String longer = s1, shorter = s2;
        if (s1.length() < s2.length()) longer = s2; shorter = s1;
        int longerLength = longer.length();
        if (longerLength == 0) return 1.0;
        return (longerLength - editDistance(longer, shorter)) / (double) longerLength;
    }

    public static void toJSONFile(Aggregate aggregate) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        File file = new File("output/" + aggregate.getName() + ".json");
        file.delete();
        mapper.writeValue(file, aggregate);
    }
    
    public static void toChart(Aggregate aggregate) throws Exception {
        ExportChart chart = new ExportChart();
        for(int i=0; i<aggregate.getFrequencies().size(); i++) {
            String name = aggregate.getName() + " - " + aggregate.getFrequencies().get(i).getName();
            Map<String, Integer> citations = aggregate.getFrequencies().get(i).getCitations();
            Map<String, Integer> hindexes = aggregate.getFrequencies().get(i).getHindexes();
            Map<String, Integer> documents = aggregate.getFrequencies().get(i).getDocumens();
            Map<String, Integer> journals = aggregate.getFrequencies().get(i).getJournalDocuments();
            Map<String, Integer> conferences = aggregate.getFrequencies().get(i).getConferenceDocuments(); 
            chart.generate(citations, "Frequency - " + name + " - Citation", "Number of citations", "Members");
            chart.generate(hindexes, "Frequency - " + name + " - H-index", "H-Index", "Members");
            chart.generate(documents, "Frequency - " + name + " - Papers", "Number of papers","Members");
            chart.generate(journals, "Frequency - " + name + " - Journal Papers", "Number of journal papers", "Members");
            chart.generate(conferences, "Frequency - " + name + " - Conference Papers", "Number of conference papers", "Members");
        }

        for(int i=0; i<aggregate.getPercentiles().size(); i++) {
            String name = aggregate.getPercentiles().get(i).getName();
            Map<String, Integer> citations_percentile = aggregate.getPercentiles().get(i).getCitations();
            Map<String, Integer> hindexes_percentile = aggregate.getPercentiles().get(i).getHindexes();
            Map<String, Integer> documents_percentile = aggregate.getPercentiles().get(i).getDocumens();
            Map<String, Integer> journals_percentile = aggregate.getPercentiles().get(i).getJournalDocuments();
            Map<String, Integer> conferences_percentile = aggregate.getPercentiles().get(i).getConferenceDocuments();
            chart.generate(citations_percentile, "Percentile - " + name + " - Citation", "Number of citations", "% of members");
            chart.generate(hindexes_percentile, "Percentile - " + name + " - H-index", "H-Index", "% of members");
            chart.generate(documents_percentile, "Percentile - " + name + " - Papers", "Number of papers","% of members");
            chart.generate(journals_percentile, "Percentile - " + name + " - Journal Papers", "Number of journal papers", "% of members");
            chart.generate(conferences_percentile, "Percentile - " + name + " - Conference Papers", "Number of conference papers", "% of members");
        }
    }
    
    public static void toCSVFileMembers(Aggregate aggregate) throws IOException {
        toCSVFile_filter(aggregate, null);
    }
    
    public static void toCSVFile(Aggregate aggregate) throws IOException {
        toCSVFile_filter(aggregate, null);
        toCSVFile_filter(aggregate, "Ricercatore");
        toCSVFile_filter(aggregate, "Associato");
        toCSVFile_filter(aggregate, "Ordinario");
        toCSVFile_frequency(aggregate);
        toCSVFile_percentiles(aggregate);
        toCSVFile_medians("output/medians-" + aggregate.getName() + ".csv", aggregate.getMedians());
    }
    
    public static void toCSVFile_percentiles(Aggregate aggregate) {
        List<String> names = new ArrayList<>();
        List<Map<String, Integer>> hindexes = new ArrayList<>();
        List<Map<String, Integer>> documents = new ArrayList<>();
        List<Map<String, Integer>> journals = new ArrayList<>();
        List<Map<String, Integer>> conferences = new ArrayList<>();
        List<Map<String, Integer>> citations = new ArrayList<>();
        for(int i=0; i<aggregate.getPercentiles().size(); i++) {
            names.add(aggregate.getPercentiles().get(i).getName());
            hindexes.add(aggregate.getPercentiles().get(i).getHindexes());
            documents.add(aggregate.getPercentiles().get(i).getDocumens());
            journals.add(aggregate.getPercentiles().get(i).getJournalDocuments());
            conferences.add(aggregate.getPercentiles().get(i).getConferenceDocuments());
            citations.add(aggregate.getPercentiles().get(i).getCitations());
        }
        Utils.toCSVFile_map("output/percentiles/percentile_hindex-" + aggregate.getName(), names, hindexes);
        Utils.toCSVFile_map("output/percentiles/percentile_papers-" + aggregate.getName(), names, documents);
        Utils.toCSVFile_map("output/percentiles/percentile_journals-" + aggregate.getName(), names, journals);
        Utils.toCSVFile_map("output/percentiles/percentile_conferences-" + aggregate.getName(), names, conferences);
        Utils.toCSVFile_map("output/percentiles/percentile_citations-" + aggregate.getName(), names, citations);    
    }
    
    public static void toCSVFile_frequency(Aggregate aggregate) throws IOException {
        List<String> names = new ArrayList<>();
        List<Map<String, Integer>> hindexes = new ArrayList<>();
        List<Map<String, Integer>> documents = new ArrayList<>();
        List<Map<String, Integer>> journals = new ArrayList<>();
        List<Map<String, Integer>> conferences = new ArrayList<>();
        List<Map<String, Integer>> citations = new ArrayList<>();
        for(int i=0; i<aggregate.getFrequencies().size(); i++) {
            names.add(aggregate.getFrequencies().get(i).getName());
            hindexes.add(aggregate.getFrequencies().get(i).getHindexes());
            documents.add(aggregate.getFrequencies().get(i).getDocumens());
            journals.add(aggregate.getFrequencies().get(i).getJournalDocuments());
            conferences.add(aggregate.getFrequencies().get(i).getConferenceDocuments());
            citations.add(aggregate.getFrequencies().get(i).getCitations());
        }
        toCSVFile_map("output/frequencies/frequency_hindex-" + aggregate.getName(), names, hindexes);
        toCSVFile_map("output/frequencies/frequency_papers-" + aggregate.getName(), names, documents);
        toCSVFile_map("output/frequencies/frequency_journals-" + aggregate.getName(), names, journals);
        toCSVFile_map("output/frequencies/frequency_conferences-" + aggregate.getName(), names, conferences);
        toCSVFile_map("output/frequencies/frequency_citations-" + aggregate.getName(), names, citations);
    }
    
    public static void toCSVFile_map(String title, List<String> names, List<Map<String, Integer>> values) {
        File file = new File(title + ".csv");
        file.delete();
        BufferedWriter writer = null;
        try  {
            writer = new BufferedWriter(new FileWriter(file, true));
            for(int i=0; i<values.size(); i++) {
                String name = names.get(i);
                String line = name + ";";
                Object[] value_names = values.get(i).keySet().toArray();
                for(int j=0; j<value_names.length; j++)
                    line += value_names[j] + ";"; 
                writer.append(line);
                writer.newLine();
                Object[] value_ints = values.get(i).values().toArray();
                line = "values;";
                for(int j=0; j<value_ints.length; j++)
                    line += value_ints[j] + ";";
                writer.append(line);
                writer.newLine();
                writer.newLine();
            }
            writer.close();              
        }
        catch (IOException ex) {
                java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally {
            try {
                writer.close();
            } 
            catch (IOException ex) {
                java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void toCSVFile_medians(String name, List<Metric> medians) throws IOException {
        File file = new File(name);
        file.delete();
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file, true));
            String line = "";
            line += "Name" + ";";
            line += "H-index" + ";";
            line += "Citations" + ";";
            line += "All Papers" + ";";
            line += "Journal Papers" + ";";
            line += "Conference Papers" + ";";
            line += "Start Year" + ";";            
            line += "Stop Year" + ";";
            writer.append(line);
            writer.newLine();
            for(int i=0; i<medians.size(); i++) {
                Metric median = medians.get(i);
                line = "";
                line += median.getName() + ";";
                line += median.getHindex() + ";";
                line += median.getCitationCount() + ";";
                line += median.getDocumentCount() + ";";
                line += median.getJournalDocumentCount() + ";";
                line += median.getConferenceDocumentCount() + ";";
                line += median.getStartYear() + ";";
                line += median.getStopYear() + ";";
                writer.append(line);
                writer.newLine();
            }
            writer.close();              
        }
        catch (IOException ex) {
                java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally {
            try {
                writer.close();
            } 
            catch (IOException ex) {
                java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
            }
        }       
    }
    
    private static void toCSVFile_filter(Aggregate aggregate, String filter) throws IOException {
        File file = null;
        if(filter == null) file = new File("output/" + aggregate.getName() + "_all.csv");
        else file = new File("output/" + aggregate.getName() + "_" + filter + ".csv");
        file.delete();
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter(file, true));
            String line = "";
            line += "ScopusID" + ";";
            line += "Name" + ";";
            line += "Surname" + ";";
            line += "Role" + ";";
            line += "Affiliation" + ";";          
            line += "Total Papers (absolute)" + ";";
            line += "Journal Papers (absolute)" + ";";
            line += "Conference Papers (absolute)" + ";";
            line += "Citations (absolute)" + ";";
            line += "H-Index (absolute)" + ";";
            line += "Total Papers (05 years)" + ";";
            line += "Journal Papers (05 years)" + ";";
            line += "Conference Papers (05 years)" + ";";
            line += "Citations (05 years)" + ";";
            line += "H-Index (05 years)" + ";";
            line += "Total Papers (10 years)" + ";";
            line += "Journal Papers (10 years)" + ";";
            line += "Conference Papers (10 years)" + ";";
            line += "Citations (10 years)" + ";";
            line += "H-Index (10 years)" + ";";
            line += "Total Papers (15 years)" + ";";
            line += "Journal Papers (15 years)" + ";";
            line += "Conference Papers (15 years)" + ";";
            line += "Citations (15 years)" + ";";
            line += "H-Index (15 years)";
            writer.append(line);
            writer.newLine();
            List<Member> members = aggregate.getMembers();
            for(int i=0; i<members.size(); i++) {
                Member member = members.get(i);
                if( (filter == null) || (member.getRole().contains(filter)) ) {
                    line = "";
                    line += member.getScopusid() + ";";
                    line += member.getName() + ";";
                    line += member.getSurname() + ";";
                    line += member.getRole() + ";";
                    line += member.getUniversity() + ";";
                    List<Metric> metrics = member.getMetrics();
                    for(int j=0; j<metrics.size(); j++) {
                        Metric metric = metrics.get(j);
                        line += metric.getDocumentCount() + ";";
                        line += metric.getJournalDocumentCount() + ";";
                        line += metric.getConferenceDocumentCount() + ";";
                        line += metric.getCitationCount() + ";";
                        line += metric.getHindex() + ";";
                    }
                    writer.append(line);
                    writer.newLine();
                }
            }
            writer.close();       
        }
        catch (IOException ex) {
                java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
        } 
        finally {
            try {
                writer.close();
            } 
            catch (IOException ex) {
                java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
            }
        }       
    }
    
    private static int editDistance(String s1, String s2)  {
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();
        int[] costs = new int[s2.length() + 1];
        for (int i = 0; i <= s1.length(); i++)  {
            int lastValue = i;
            for (int j = 0; j <= s2.length(); j++) {
                if (i == 0) costs[j] = j;
                else {
                    if (j > 0) {
                        int newValue = costs[j - 1];
                        if (s1.charAt(i - 1) != s2.charAt(j - 1))
                            newValue = Math.min(Math.min(newValue, lastValue), costs[j]) + 1;
                        costs[j - 1] = lastValue;
                        lastValue = newValue;
                    }
                }
            }
            if (i > 0) costs[s2.length()] = lastValue;
        }
        return costs[s2.length()];
    }
}
