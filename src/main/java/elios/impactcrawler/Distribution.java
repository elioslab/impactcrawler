package elios.impactcrawler;

import java.io.Serializable;
import java.util.Map;

public class Distribution implements Serializable {
    private final String name;
    private final Integer startYear;
    private final Integer stopYear;
    private final Map<String, Integer> hindexes;
    private final Map<String, Integer> documens;
    private final Map<String, Integer> journalDocuments;
    private final Map<String, Integer> conferenceDocuments;
    private final Map<String, Integer> citations;

    public Distribution(String name, Integer startYear, Integer stopYear, Integer index, Map<String, Integer> hindexes, Map<String, Integer> documens, Map<String, Integer> journalDocuments, Map<String, Integer> conferenceDocuments, Map<String, Integer> citations) {
        this.name = name;
        this.hindexes = hindexes;
        this.documens = documens;
        this.journalDocuments = journalDocuments;
        this.conferenceDocuments = conferenceDocuments;
        this.citations = citations;
        this.startYear = startYear;
        this.stopYear = stopYear;
    }

    public String getName() { return name; }
    public Integer getStartYear() { return startYear; }
    public Integer getStopYear() { return stopYear; }
    public Map<String, Integer> getHindexes() { return hindexes; }
    public Map<String, Integer> getDocumens() { return documens; }
    public Map<String, Integer> getJournalDocuments() { return journalDocuments; }
    public Map<String, Integer> getConferenceDocuments() { return conferenceDocuments; }
    public Map<String, Integer> getCitations() { return citations; }
    
    @Override
    public String toString() {
        String description = "Distribution{";
        description += "name:" + name + "; ";
        description += "Start year:" + startYear + "; ";
        description += "Stop Year:" + stopYear + "; \n";
        Object[] keys = hindexes.keySet().toArray();
        Object[] values = hindexes.values().toArray();
        description += "H-Index:{ ";
        for(int i=0; i<hindexes.size(); i++) description += "(" + keys[i] + "," + values[i] + "), "; 
        description += "}; \n";
        keys = documens.keySet().toArray();
        values = documens.values().toArray();
        description += "All papers:{ ";
        for(int i=0; i<documens.size(); i++) description += "(" + keys[i] + "," + values[i] + "), "; 
        description += "}; \n";
        keys = journalDocuments.keySet().toArray();
        values = journalDocuments.values().toArray();
        description += "Journal papers:{ ";
        for(int i=0; i<journalDocuments.size(); i++) description += "(" + keys[i] + "," + values[i] + "), "; 
        description += "}; \n";
        keys = conferenceDocuments.keySet().toArray();
        values = conferenceDocuments.values().toArray();
        description += "Conference papers:{ ";
        for(int i=0; i<conferenceDocuments.size(); i++) description += "(" + keys[i] + "," + values[i] + "), "; 
        description += "}; \n";
        keys = citations.keySet().toArray();
        values = citations.values().toArray();
        description += "Citations:{ ";
        for(int i=0; i<citations.size(); i++) description += "(" + keys[i] + "," + values[i] + "), "; 
        description += "}; \n";
        description += "}";
        return description;
    }
}
