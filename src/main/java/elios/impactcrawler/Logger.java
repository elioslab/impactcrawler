package elios.impactcrawler;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;


public class Logger {
    private static File file = null;
    private static File warningfile = new File("output/logs/warning.txt");
    private static Boolean verbose = false;
    
    public static void setFileName(String fileName) {
        if(fileName == null)
            file = null;
        else {
            file = new File(fileName);
            file.delete();
        }
    }

    public static void setVerbose(Boolean verbose) { Logger.verbose = verbose; }
   
    public static void write(String message) {
        writeOnFile(message);
        writeOnConsole(message);
    }
    
    public static void warning(String message) {
        writeOnWarningFile("[" + Utils.getDate() + "] " + message);
    }
    
    private static void writeOnConsole(String message) {
        if(verbose == true)
            System.out.println(message);
    }
    
    private static void writeOnWarningFile(String message) {
        if(file != null) {
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new FileWriter(warningfile, true));
                writer.append(message);
                writer.newLine();
                writer.close();
            } 
            catch (IOException ex) {
                java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
            } 
            finally {
                try {
                    writer.close();
                } 
                catch (IOException ex) {
                    java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
    
    private static void writeOnFile(String message) {
        if(file != null) {
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new FileWriter(file, true));
                writer.append(message);
                writer.newLine();
                writer.close();
            } 
            catch (IOException ex) {
                java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
            } 
            finally {
                try {
                    writer.close();
                } 
                catch (IOException ex) {
                    java.util.logging.Logger.getLogger(Logger.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }
}
