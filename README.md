# README

L’applicazione ImpactCrawler ha lo scopo di permettere l’estrazione di informazioni di impatto scientifico (h-index, numero di pubblicazioni, citazioni) e didattiche (numero di laureati) dai database Scopus, Cineca e MIUR e di calcolare indicazioni statistiche (mediane, distribuzioni) per una popolazione definita (SSD) della comunità universitaria italiana.

## Descrizione dell’algoritmo

L'algoritmo implementato in ImpactCrawler per la misura delle informazioni di impatto scientifico e didattico è composto dai seguenti passi:

1. L’algoritmo interroga  il database [MIUR “Portale dei dati dell'istruzione superiore”][1], che contiene l’elenco degi laureati nelle Università italiane. La ricerca è impostata inserendo un insieme di ID di classi di laurea (ad esempio “LM-29”, "LM-32"). Il software estrae così il numero di laureati per quelle classi.

2. L’algoritmo interroga  il database [Cineca “Cerca Università”][2], che contiene l’elenco dei docenti delle Università italiane. La ricerca è impostata inserendo un inisieme di SSD (ad esempio “ING-INF/01”, "ING-INF/05"). Purtroppo il sistema Cineca non è pensato per una interfaccia “machine readable”, così il software compone una richiesta POST analizzando il contenuto della pagina e estrae le informazioni sull’elenco dei docenti effettuando il parser della pagina HTML restituita dal sito. Il log delle informazioni ottenute è esportato nel file “log_members_cineca_SSD.txt”. E’ possibile estrarre analoghe informazioni per altri SSD (basta utilizzare una diversa stringa per l’interrogazione di Cineca all'interno del software) o per altri aggregati (ad esempio, Università o Dipartimenti) utilizzando diverse chiavi di ricerca.

3. Utilizzando l’elenco dei docenti ottenuto al punto 2), l’algoritmo interroga le API messe a disposizione da [Elsevier][3]. Per l’utilizzo di queste API è necessario ottenere una API key e collegarsi da un IP di una istituzione con un abbonamento Scopus. Per questo il software realizzato, per poter essere usato da altri, necessità l’inserimento di una key valida e di una postazione con IP valido. L’interrogazione delle API ha lo scopo di individuare lo "Scopus ID" univoco di ogni docente. Per questo viene effettuata una ricerca utilizzando cognome e nome del docente e risolvendo le omonimie utilizzando l’istituzione di provenienza e le aree di interesse scientifico. Il log delle informazioni ottenute è esportato nel file “log_ids_scopus_SSD.txt”. Queste informazioni sono affette da rumore, poiché non è certa l’attribuzione del giusto Scopus ID per ogni docente estratto da Cineca. Vi possono essere diversi casi dubbi che sono indicati nel file “warning.txt”.

4. Utilizzando l’elenco degli ScopusID ottenuto al punto 3), l’algoritmo interroga nuovamente le API Elsevier per ottenere per ogni docente l’elenco delle sue pubblicazioni con le seguenti informazioni: identificativo scopus, titolo, numero di citazioni, anno di pubblicazione e tipo di pubblicazione (rivista/conferenza/altro). Il log delle informazioni ottenute è esportato nel file “log_papers_scopus_SSD.txt”

5. L’algoritmo elabora le informazioni ottenute al punto 4) per costruire alcune metriche (h-index, numero di documenti totale, numero di pubblicazioni su rivista, numero di pubblicazioni su conferenza, h-index) per ogni docente e per diversi periodi di tempo: assoluto, ultimi 10 anni, ultimi 15 anni. Il log delle informazioni ottenute è esportato nel file “log_metrics_evaluation_SSD.txt”.

6. L'algoritmo elabora le informazioni ottenute al punto 5) per determinare alcuni dati statistici aggregati (mediane, distribuzioni e percentili), su alcune sotto-popolazioni (tutti i docenti, solo i ricercatori, solo gli associati, solo gli ordinari) e per diversi periodi temporali (sempre, ultimi 10 anni, ultimi 15 anni). I dati ottenuti sono esportati nel file "log_medians_evaluation_SSD.txt", "log_distributions_evaluation_SSD.txt" e "log_percentile_evaluation_SSD.txt"

7. Infine l’algoritmo esporta i dati in vari formati di tipo "machine readable": 
- viene generato un file JSON con tutte le informazioni raccolte e che può essere importato in altri strumenti per successive elaborazioni (vedi file “ssd.json”), ad esempio importando la stringa contenuta nel file nello strumento online [jsonviewer][4]; 
- viene generata una serie di files CSV (ssd_all.csv, ssd_associati.csv, ssd_ricercatori.csv, ssd_ordinari.csv) che contengono i dati prodotti ai punti precedenti, in modo che possano essere analizzati per ulteriori elaborazioni statistiche;
- viene generato un file CSV (medians-SSD.csv) che contiene i valori delle mediane per i vari indicatori di impatto
- viene generata una serie di files CSV (distribution_citations_SSD.csv, distribution_conferences_SSD.csv, distribution_hindex_SSD.csv, distribution_journals_SSD.csv, distribution_papers_SSD.csv) che riportano i valori delle varie distribuzioni calcolate
- anche per i percentili vengono generati gli stessi file delle distribuzioni
- per ogni distribuzione e per ogni percentile viene prodotto un grafico in formato jpeg.

Per ulteriori informazioni contattare riccardo.berta[at]unige.it

[1]:	http://ustat.miur.it/
[2]:	http://cercauniversita.cineca.it/php5/docenti/cerca.php
[3]:	http://dev.elsevier.com/index.html
[4]:	http://jsonviewer.stack.hu/

